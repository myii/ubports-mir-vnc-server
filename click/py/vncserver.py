import atexit
import os
import pyotherside
import subprocess

from threading import Thread


running = False
processes = []


def start(displayId, viewOnly, width, height, refreshrate, rotation):
    p = subprocess.Popen(
        [
            "/opt/click.ubuntu.com/mirvncserver.abmyii/current/vncserver.sh",
            "-m", "/run/mir_socket",
            "-d", displayId,
            viewOnly,
            "-s", width, height,
            "--cap-interval", refreshrate,
            "-o", rotation
        ],
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )
    processes.append(p)

    running = True

    thread = Thread(target=log_worker, args=[p.stdout])
    thread.daemon = True
    thread.start()

    thread.join(timeout=1)

    return p


def log_worker(stdout):
    ''' needs to be in a thread so we can read the stdout w/o blocking '''
    for stdout_line in iter(stdout.readline, ''):
        if len(stdout_line) > 0:
            pyotherside.send('log', stdout_line)


def stop(process):
    if process != None:
        process.kill()
        running = False
        return True
    else:
        return False


def check(process):
    if process != None and process.poll():
        running = False
        return False
    else:
        return True


@atexit.register
def stop_subprocesses():
    # Ensure all VNC server processes are terminated
    for p in processes:
        p.kill()
